package main

import (
	"fmt"
	"net/http"
	"os"
)

func main() {
	// serve files
	http.HandleFunc("/", ServeFiles)
	// listen on port
	port := "8080"
	// listen on port from environment if set
	if len(os.Args) > 1 && os.Args[1] == "--port" {
		port = os.Args[2]
	}
	fmt.Println("Serving on port", port)
	// start server
	http.ListenAndServe(":"+port, nil)
}

// ServeFiles serves files from the given directory
func ServeFiles(w http.ResponseWriter, r *http.Request) {
	// get file path
	path := r.URL.Path[1:]
	if path == "" {
		// for /, serve index.html
		path = "index.html"
	} else {
		// anything else, serve file
		path = "./" + path
	}
	http.ServeFile(w, r, path)
}
