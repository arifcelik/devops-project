// this variales declaration on the pipeline
variable "AWS_ACCESS_KEY" {

}

variable "AWS_SECRET_KEY" {

}

variable "AWS_REGION" {
  default = "eu-central-1"
}

variable "REGISTRY" {
  
}

variable "IMAGENAME" {
  
}

variable "TAG" {
  
}

variable "AMIS" {
  type = map(string)
  default = {
    eu-central-1 = "ami-065deacbcaac64cf2"
  }
}

variable "sg-ingress-rules" {
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_block  = string
    description = string
  }))
  default = [
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_block  = "0.0.0.0/0"
      description = "in 80"
    },
    {
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      cidr_block  = "0.0.0.0/0"
      description = "in 443"
    },
  ]
}
