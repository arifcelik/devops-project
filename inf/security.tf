// create all security groups for vpcs and avibility zones
resource "aws_security_group_rule" "ingress-rules" {
  count = length(var.sg-ingress-rules)

  type              = "ingress"
  from_port         = var.sg-ingress-rules[count.index].from_port
  to_port           = var.sg-ingress-rules[count.index].to_port
  protocol          = var.sg-ingress-rules[count.index].protocol
  cidr_blocks       = [var.sg-ingress-rules[count.index].cidr_block]
  description       = var.sg-ingress-rules[count.index].description
  security_group_id = aws_security_group.lbs.id
}

resource "aws_security_group_rule" "egress-rules" {

  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = -1
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "egress"
  security_group_id = aws_security_group.lbs.id
}

resource "aws_security_group_rule" "task-ingress" {
  type = "ingress"
  from_port = 80
  to_port = 80
  protocol = "tcp"
  description = "task ingress"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.tasks-sg.id
}

resource "aws_security_group_rule" "task-egress" {
  type = "egress"
  from_port = 0
  to_port = 0
  protocol = -1
  description = "task ingress"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.tasks-sg.id
}

resource "aws_security_group" "lbs" {
  name   = "bootcamp-proje-ingress-lbs"
  description = "load balancer ingress and egress"
  vpc_id = aws_vpc.vpc.id
  tags = {
    "bootcamp" = "proje"
  }
}

resource "aws_security_group" "tasks-sg" {
  name = "bootcamp-proje-tasks-sg"
  description = "80 port for alb, outbound all"
  vpc_id = aws_vpc.vpc.id

  tags = {
    "bootcamp" = "proje"
  }
}
