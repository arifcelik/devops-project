// use load balancer in front of the service
resource "aws_alb" "load-balancer" {
  name               = "bootcamp-proje-load-balancer"
  load_balancer_type = "application"
  subnets            = [aws_subnet.subnet-a.id, aws_subnet.subnet-b.id]
  security_groups    = [aws_security_group.lbs.id]

  tags = {
    "bootcamp" = "proje"
  }
}

resource "aws_alb_target_group" "lb-target-group" {
  name = "bootcamp-proje"
  port = 80
  protocol = "HTTP"
  vpc_id = aws_vpc.vpc.id
  target_type = "ip"
}

resource "aws_alb_listener" "lb-listener" {
  load_balancer_arn = aws_alb.load-balancer.arn
  port = 80
  protocol = "HTTP"
  default_action {
    type = "forward"
    target_group_arn = aws_alb_target_group.lb-target-group.arn
  }
}
