// create a cluster
resource "aws_ecs_cluster" "ecs-cluster" {
  name = "bootcamp-proje-ecs-cluster"
}

// create a fargate service
resource "aws_ecs_service" "ecs-service" {
  name = "bootcamp-proje-ecs-service"
  cluster = aws_ecs_cluster.ecs-cluster.id
  task_definition = aws_ecs_task_definition.ecs-task-definition.arn
  launch_type = "FARGATE"

  // set subnets and security groups
  network_configuration {
    subnets = [ aws_subnet.subnet-a.id, aws_subnet.subnet-b.id ]
    assign_public_ip = true
    security_groups = [ aws_security_group.lbs.id, aws_security_group.tasks-sg.id ]
  }

  // set desired count 2 to scale up and down
  desired_count = 2

  // set load balancer
  load_balancer {
    container_name = "lb"
    container_port = 80
    target_group_arn = aws_alb_target_group.lb-target-group.arn
  }

  depends_on = [
    aws_alb_listener.lb-listener
  ]
}

resource "aws_ecs_task_definition" "ecs-task-definition" {
  family = "bootcamp-proje"
  network_mode = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  memory = 512
  cpu = 256
  container_definitions = <<EOF
  [
    {
      "name": "proje",
      "image": "${var.REGISTRY}/${var.IMAGENAME}:${var.TAG}",
      "cpu": 256,
      "memory": 512,
      "essential": true,
      "portMappings": [
        {
          "containerPort": 80,
          "hostPort": 80,
          "protocol": "tcp"
        }
      ]
    }
  ]
  EOF
}
