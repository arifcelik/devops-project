// create vpc security group for tasks and subnets
resource "aws_vpc" "vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  tags = {
    "bootcamp" = "proje"
  }
}

resource "aws_subnet" "subnet-a" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = "10.0.1.0/28"
  availability_zone = "${var.AWS_REGION}a"
  tags = {
    "bootcamp" = "proje"
  }
}

resource "aws_subnet" "subnet-b" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = "10.0.2.0/28"
  availability_zone = "${var.AWS_REGION}b"
  tags = {
    "bootcamp" = "proje"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    "bootcamp" = "proje"
  }
}

resource "aws_route_table" "rt" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    "bootcamp" = "proje"
  }
}

resource "aws_route_table_association" "rta1" {
  subnet_id      = aws_subnet.subnet-a.id
  route_table_id = aws_route_table.rt.id
}

resource "aws_route_table_association" "rta2" {
  subnet_id      = aws_subnet.subnet-b.id
  route_table_id = aws_route_table.rt.id
}
