#!/bin/bash

push() {
    echo "Pushing to gitlab..."
    echo -n "Enter the message : "
    read message
    echo -n "Enter the tag (empty for no tag) : "
    read tag

    git add .
    git commit -m "$message"
    git push

    if [[ -n $tag ]]; then
        git tag $tag
        git push origin $tag
    fi
}

npm install
npm run build

echo "Build complete"
echo
echo -n "push to gitlab ? (y/n) : "
read -n 1 prompt
echo
[[ $prompt == "y" ]] && push || echo "Push aborted"
