# first, i use node image for build the react app
FROM node:lts-slim as builder

# use app directory
WORKDIR /app
# and copy package.json and package-lock.json to the app directory
COPY ["package.json", "package-lock.json", "./"]
# install dependencies
RUN npm install
# copy the react app to the app directory
COPY . .
# build the app
RUN npm run build

# second, i use go image because a go file can be compiled independently of the dependencies
# so this this will come in handy in the later
FROM golang:1.14-alpine as runtime

# use src directory
WORKDIR /src
# copy go.mod and main.go files
COPY main.go ./
# init the go project
RUN go mod init app
# and download dependencies if needed
RUN go mod download
# build the app without dependencies
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o main
# now we can use scratch image becase we don't need any platform and dependencies
# so this makes the image smaller
FROM scratch

# use app directory
WORKDIR /app
# cop build directory
COPY --from=builder /app/build .
# and copy main binary to the app directory
COPY --from=runtime /src/main .

# serve on port 8080
EXPOSE 8080

# run the app
ENTRYPOINT [ "./main" ]