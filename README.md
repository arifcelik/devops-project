# Protein DevOps Bootcamp Final Project
This repo created for Protein Bootcamp Final Project. 

## The Steps of the Project
  
### Create React App and Dokcerimage
First i created a react app and docker image. Dockerfile basically was like this:
```dockerfile
FROM node:latest
COPY . .
RUN npm install
RUN npm run build
CMD ["serve", "-s", "build"]
```

That wasn't nice. First thing i did was use nginx:alpine image. This was better but not enough. What I did to reduce the image size is simply build the application, then publish it in go language and build again for run without ependency, finally use it in the scratch image. Thus the size down to 4.5MB.

### Use Gitlab Runner
I tried the shared runners first, but since installing it on my own computer is better in terms of performance, I installed it on my computer.  
In Gitlab Pipeline i used kaniko for build the image. It's simply build image from dockerfile and pushes it to AWS ECR.

### Use Terraform for Infrastructure
while pipeline is running and build job completed, i use terraform for infrastructure and install services. It's uses aws provider resources. For this, I made use of their own sites and google as much as possible 🙂.  
In the second part of the pipeline, I assign the necessary variables for terraform and do the `terraform init`, `terraform plan` and `terraform apply` operations respectively.

>
>Unfortunately, I could not deploy the project as it is 😢. When i tried to deploy the project, i got the following error:
>```
>Error: failed creating ECS Task Definition (bootcamp-proje): ClientException: Fargate requires task definition to have execution role ARN to support ECR images.
>```
>From what I've researched, it was said to be a bug with AWS' new design, but I haven't been able to resolve the issue.